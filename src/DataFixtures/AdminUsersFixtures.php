<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminUsersFixtures extends Fixture
{
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        foreach ($this->getUserData() as [$email, $first_name, $last_name, $roles, $password])
        {
            $user = new User();
            $user->setEmail($email);
            $user->setFirstName($first_name);
            $user->setLastName($last_name);
            $user->setRoles($roles);
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user, $password
            ));
            $manager->persist($user);
        }

        $manager->flush();
    }

    private function getUserData(): array
    {
        return [
            ['rodrigo@gmail.com', 'Rodrigo', 'Wayne', ['ROLE_ADMIN'], '123456'],
            ['leticia@gmail.com', 'Leticia', 'Wayne', ['ROLE_ADMIN'], '123456'],
            ['larissa@gmail.com', 'Larissa', 'Wayne', ['ROLE_ADMIN'], '123456'],
        ];
    }
}
