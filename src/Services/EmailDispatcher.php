<?php


namespace App\Services;


use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class EmailDispatcher
{
    const NEWSLETTER_FROM = 'no-reply@blogproject.com';
    const NEWSLETTER_SUBJECT = 'Subscription Confirmation';
    const NEWSLETTER_TEMPLATE = 'auto-email/reply.html.twig';
    const NEWSLETTER_NAME = 'NO-REPLY';

    const CONTACTS_FROM = 'contact@blogproject.com';
    const CONTACTS_SUBJECT = 'Thank you for your contact';
    const CONTACTS_TEMPLATE = 'email/reply.html.twig';
    const CONTACT_NAME = 'Blog Project';
    const CONTACTS_REPLY_TO = 'contact@blogproject.com';

    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * @var TemplatedEmail
     */
    private $templatedEmail;


    public function __construct(MailerInterface $mailer, TemplatedEmail $templatedEmail)
    {
        $this->mailer = $mailer;
        $this->templatedEmail = $templatedEmail;
    }

    public function sendContactEmail(string $email, string $name): bool
    {
        $addressFrom = new Address(self::CONTACTS_FROM, self::CONTACT_NAME);
        $addressTo = new Address($email, $name);

        $this->templatedEmail
            ->from($addressFrom)
            ->to($addressTo)
            ->subject(self::CONTACTS_SUBJECT)
            ->replyTo(self::CONTACTS_REPLY_TO)
            ->htmlTemplate(self::CONTACTS_TEMPLATE);
        try {
            $this->mailer->send($this->templatedEmail);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return true;

    }
}