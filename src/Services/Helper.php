<?php


namespace App\Services;


class Helper
{
    public static function inputSanitizer(string $input)
    {
        $input = trim($input);
        $input = stripcslashes($input);
        $input = htmlspecialchars($input);
        return $input;
    }

    public static function especialDecode(string $input)
    {
        return htmlspecialchars_decode($input);

    }
}