<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Repository\ContactRepository;
use App\Services\EmailDispatcher;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Services\Helper;


class ContactController extends AbstractController
{
    const CONTACT_CONFIRMATION = 'Obrigado por entrar em contato conosco';

    /**
     * @Route("admin/contact/contact-list", name="contact_index", methods={"GET"})
     */
    public function index(ContactRepository $contactRepository): Response
    {

        $contacts = $contactRepository->findAll();

        //TODO: decode special char;

        return $this->render('admin/contact/index.html.twig', [
            'contacts' => $contacts,
        ]);
    }

    /**
     * @Route("/new-contact", name="contact_new", methods={"GET","POST"})
     * @param Request $request
     * @param EmailDispatcher $emailDispatcher
     * @return Response
     * @throws \Exception
     */
    public function new(Request $request, EmailDispatcher $emailDispatcher): Response
    {


//        $form = $this->createForm(Contact::class);
//
//        $form->handleRequest($request);
//        if ($form->isSubmitted() && $form->isValid()) {
//
//        }

        $form_data = $request->request->all();
        $isSubmited = isset($form_data['name']);

        if ($isSubmited) {
            $name = Helper::inputSanitizer($form_data['name']);
            $email = Helper::inputSanitizer($form_data['email']);
            $subject = Helper::inputSanitizer($form_data['subject']);
            $message = Helper::inputSanitizer($form_data['message']);

            $contact = new Contact();
            $contact->setName($name);
            $contact->setEmail($email);
            $contact->setSubject($subject);
            $contact->setMessage($message);
            $contact->setCreatedAt(new \DateTime('NOW'));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contact);
            $entityManager->flush();


            try {
                $this->addFlash('success', self::CONTACT_CONFIRMATION);
                $emailDispatcher->sendContactEmail($contact->getEmail(), $contact->getName());

            } catch (\Exception $e) {
                $e->getMessage();
            }

        }

        return $this->redirect('contact');


    }

    /**
     * @Route("admin/contact/{id}", name="contact_show", methods={"GET"})
     */
    public function show(Contact $contact): Response
    {
        return $this->render('admin/contact/show.html.twig', [
            'contact' => $contact,
        ]);
    }

    /**
     * @Route("admin/contact/{id}/edit", name="contact_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Contact $contact): Response
    {
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('contact_index');
        }

        return $this->render('admin/contact/edit.html.twig', [
            'contact' => $contact,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("admin/contact/{id}", name="contact_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Contact $contact): Response
    {
        if ($this->isCsrfTokenValid('delete'.$contact->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($contact);
            $entityManager->flush();
        }

        return $this->redirectToRoute('contact_index');
    }
}
