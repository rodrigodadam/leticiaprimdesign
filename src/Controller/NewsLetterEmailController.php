<?php

namespace App\Controller;

use App\Entity\NewsLetterEmail;
use App\Form\NewsLetterEmailType;
use App\Repository\NewsLetterEmailRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class NewsLetterEmailController extends AbstractController
{
    const NEWS_LETTER_ERROR = 'E-mail ja cadastrado';
    const NEWS_LETTER_CONFIRMATION = 'Obrigado por assinar nossas News';



    /**
     * @Route("/news-letter", name="news_letter", methods={"GET","POST"})
     */
    public function new(Request $request)
    {

        $form_data = $request->request->all();

        $newsLetterEmail = new NewsLetterEmail();
        $newsLetterEmail->setEmail($form_data['email']);
//        $form = $this->createForm(NewsLetterEmailType::class, $newsLetterEmail);
//        $form->handleRequest($request);

//        if ($newsLetterEmail->isSubmitted() && $newsLetterEmail->isValid()) {
        try {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($newsLetterEmail);
            $entityManager->flush();
            $this->addFlash('success', self::NEWS_LETTER_CONFIRMATION);
        } catch (\Exception $e) {
            $error = $e->getMessage();
            $this->addFlash('error', self::NEWS_LETTER_ERROR . $error);
        }

        return $this->redirect("/");
//        $previousRoute = $request->server->get('HTTP_REFERER');
//        $routeToRedirect = explode("/", $previousRoute);
//        $routeToRedirect[3] = $routeToRedirect[3] === "" ? "app_homepage" : $routeToRedirect[3];
//
//        //TODO: Mensagem de erro para email ja cadastrado.
//        return $this->redirectToRoute($routeToRedirect[3]);
//        }

//        return $this->render('news_letter_email/new.html.twig', [
//            'news_letter_email' => $newsLetterEmail,
//            'form' => $newsLetterEmail->createView(),
//        ]);
    }

    /**
     * @Route("admin/news-letter/news-letter-list", name="news_letter_email_index", methods={"GET"})
     */
    public function index(NewsLetterEmailRepository $newsLetterEmailRepository)
    {
        return $this->render('admin/news_letter_email/index.html.twig', [
            'news_letter_emails' => $newsLetterEmailRepository->findAll(),
        ]);
    }

    /**
     * @Route("admin/news-letter/{id}", name="news_letter_email_show", methods={"GET"})
     */
    public function show(NewsLetterEmail $newsLetterEmail): Response
    {
        return $this->render('admin/news_letter_email/show.html.twig', [
            'news_letter_email' => $newsLetterEmail,
        ]);
    }

    /**
     * @Route("admin/{id}/edit", name="news_letter_email_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, NewsLetterEmail $newsLetterEmail): Response
    {
        $form = $this->createForm(NewsLetterEmailType::class, $newsLetterEmail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin/news_letter_email_index');
        }

        return $this->render('admin/news_letter_email/edit.html.twig', [
            'news_letter_email' => $newsLetterEmail,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("admin/news-letter/{id}", name="news_letter_email_delete", methods={"DELETE"})
     */
    public function delete(Request $request, NewsLetterEmail $newsLetterEmail): Response
    {
        if ($this->isCsrfTokenValid('delete'.$newsLetterEmail->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($newsLetterEmail);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin/news_letter_email_index');
    }
}
